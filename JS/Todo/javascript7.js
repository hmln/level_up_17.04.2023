const OPTION_VALUE = {
  all: "all",
  completed: "Completed",
  notComplited: "notComplited",
}

class Model{
  constructor(){
    this.todos = [];
    this.completedTodo = 0;
    this.optionValue = OPTION_VALUE.all;
  }

  addTodo(inputText) {
    const todo = {
      id: Date.now(),
      text: inputText,
      complete: false,
    };

    this.todos.push(todo);

    this.renderTodo(this.todos);
  }

  bindTodoListChanged(callback) {
    this.onTodoListChanged = callback;
  }

  editComplete(todoId) {
    this.todos = this.todos.map((item) => 
      item.id === todoId ? {...item, complete: !item.complete } : item
    ); 
    this.renderTodo(this.todos);
  }

  editText(todoId, text) {
    this.todos = this.todos.map((item) => 
      item.id === todoId ? {...item, text } : item
    );

    this.renderTodo(this.todos);
  }

  removeTodo(todoId){
    this.todos = this.todos.filter((item) => item.id !== todoId);

    this.renderTodo(this.todos);
  }

  percentComplete(){
    const filterCompleteTodo = this.todos.filter((item) => item.complete);
    const completeCompleteResult = 
    (100*filterCompleteTodo.length)/this.todos.length;
    
    this.completedTodo = completeCompleteResult;
  }

  deleteTodos (){
    this.todos = [];
    localStorage.clear("todos")
    this.renderTodo(this.todos);
  }

  filterTodo(optValue){
    const filterTodo = this.todos.filter((item) => {
      if (optValue === OPTION_VALUE.all) return true;
      if (optValue === OPTION_VALUE.completed) {
        return item.complete;
      }
      if (optValue === OPTION_VALUE.notComplited) {
        return !item.complete;
      }
    });

    this.renderTodo(filterTodo);
  }

  searchTodo(inputSearchValue) {
    const searchTodo = this.todos.filter((item) => 
    item.text
    .toLocaleLowerCase()
    .includes(inputSearchValue.toLocaleLowerCase())
    );
    this.renderTodo(searchTodo)
  }

  renderTodo(todos) {
    this.percentComplete();
    this.onTodoListChanged(todos, this.completedTodo);
  }

  saveLocalStorageTodo() {
    const jsonStringifyTodos = JSON.stringify(this.todos);
    localStorage.setItem("todos", jsonStringifyTodos);
  }

  getLocaleStorageTodos(){
    const getTodos = localStorage.getItem("todos");
    const parseTodos = JSON.parse(getTodos) || [];
    this.todos = parseTodos;
    this.removeTodo(this.todos);
  }
}

class View{
  constructor(){
    this.app = this.getElement("#root");
    // создем элементы на страниице
    this.form = this.createElement('form');
    this.progressBar = this.createElement("div", "progressbar");
    this.progressBarResult = this.createElement("div", "progressbar__result");
    this.header = this.createElement("header", "header");
    this.input = this.createElement('input', "form__input");
    this.buttonAdd = this.createElement("button", "form__button");
    this.lists = this.createElement("ul", "lists");
    this.title = this.createElement("h1", "title");
    this.inputWrapper = this.createElement("div", "input__wrapper")
    this.inputSearch = this.createElement("input", "input__search");
    this.inpeutSearchWrapper = this.createElement("div", "input__search-wrapper");
    this.wrapperHeader = this.createElement("div", "header__wrapper");
    this.select = this.createElement("select", "select");
    this.buttonDeleteAll = this.createElement("button", "button__all");
    this.buttonSaveLocalStorage = this.createElement(
      "button",
      "button__save-ls"
      );

    this.options = [
      {value: OPTION_VALUE.all, text: "All"},
      {value: OPTION_VALUE.completed, text: "Completed"},
      {value: OPTION_VALUE.notComplited, text: "Not Complited"}
    ];

    for (let i=0; i < this.options.length; i++) {
      const opt = this.createElement("option");
      opt.value = this.options[i].value;
      opt.innerHTML = this.options[i].text;
      this.select.appendChild(opt);
    };

    //input search

    this.progressBarResultPercent = this.createElement(
      "p",
      "progressbap__percent"
      );
    
    this.wrapperHeader.append(this.select, this.buttonDeleteAll);    
    this.inputWrapper.append(this.input, this.buttonAdd);
    this.inpeutSearchWrapper.append(
      this.inputSearch, 
      this.buttonSaveLocalStorage
    );
    this.form.append( this.inputWrapper, this.inpeutSearchWrapper);
    this.header.append(this.title,this.wrapperHeader);
    this.progressBar.append(this.progressBarResult, this.progressBarResultPercent);
    //точка входа приложения
    this.app.append(this.progressBar, this.header, this.form, this.lists);

    this.input.placeholder = "Добавить";
    this.input.type = "text";

    this.inputSearch.placeholder = "Поиск";
    this.inputSearch.type = "text";

    //Add content
    this.title.textContent = "Чек-лист";
    this.buttonDeleteAll.textContent = "Удалить всё";
    this.buttonAdd.textContent = "Добавить запись";
    this.buttonSaveLocalStorage.textContent = "Сохранить чек-лист"
    
    this.spanText = "";
    this.InitListeners();
  }
 
  get _getInputValue() {
    return this.input.value;
  }

  InitListeners(){
    this.lists.addEventListener("input", (event) => {
      if (event.target.className === "editable") {
        this.spanText = event.target.innerText;
      }
    });
  }

  renderTodo(todos, completedTodo) {
    while(this.lists.firstChild) {
      this.lists.removeChild(this.lists.firstChild);
    }
    if (todos.length) {
      todos.map((item) => {
        const li = this.createElement("li", "list");
        const span = this.createElement("span");
        const checkbox = this.createElement("input");
        const buttonRemove = this.createElement("button", "button__remove");
        this.progressBarResult.style.width = `${completedTodo}%`;
        this.progressBarResultPercent.textContent = `${completedTodo.toFixed(1)}%`;
        
        checkbox.type = "checkbox";
        checkbox.checked = item.complete;
        buttonRemove.textContent = "Удалить запись"
       
        li.id = item.id;
        span.contentEditable = true;
        span.classList.add("editable")

        if(item.complete) {
          const strike   = this.createElement("s");
          strike.textContent = item.text;
          span.append(strike);
        } else {
          span.textContent = item.text;
        }

        li.append(span, buttonRemove, checkbox);
        this.lists.append(li);
      });
    } else {
      const p = this.createElement("p", "title__hint");
      p.textContent = "Список пуст!";
      this.lists.append(p);
    }
      }

  resetInput() {
    this.input.value = "";
  }

  bindAddTodo(callback) {
    this.form.addEventListener("submit", (event) => {
      event.preventDefault();
      if(this._getInputValue){
        callback(this._getInputValue);
        this.resetInput();
      }
    });
  }

  bindToggleCheckbox(callback) {
    this.lists.addEventListener("change", (event) =>{
      if(event.target.type === "checkbox"){
        callback(parseInt(event.target.parentElement.id));
      }
    });
  }

  bindEditTextInput(callback) {
    this.lists.addEventListener("focusout", (event) => {
      if(this.spanText) {
        const todoId = parseInt(event.target.parentElement.id);
        callback(todoId, this.spanText);
      }
    });
  }

  bindRemoveTodo(callback) {
    this.lists.addEventListener("click",({ target }) => {
      if(target.className === "button__remove"){
        const id =  parseInt(target.parentElement.id);
        callback(id);
      }
    });
  }

  bindFilterTodo(callback) {
    this.select.addEventListener("click",() => {
      const optionIndex = this.select.selectedIndex;
      const selectedOption = this.select.options;
      callback(selectedOption[optionIndex].value)
    })
  }
  
  bindDeleteAllTodo (callback) {
    this.buttonDeleteAll.addEventListener("click",() => {
      callback()
    });
  }

  bindSearchTextTodo (callback){
    this.inputSearch.addEventListener("change", (e) =>{
      callback(e.target.value)
    });
  }

  bindSaveLocalStorage(callback) {
    this.buttonSaveLocalStorage.addEventListener("click", () => {
      callback()
    })
  }

  getElement(selector) {
    const element = document.querySelector(selector);
    return element;
  }

  createElement(tag, className) {
    const element = document.createElement(tag);
    if(className) element.classList.add(className);
    return element;
  }
}

class Controller{
  constructor(model, view){
    this.model = model;
    this.view = view;
    this.model.bindTodoListChanged(this.onTodoListChanged);
    this.view.bindAddTodo(this.handleAddTodo);
    this.view.bindToggleCheckbox(this.handleEditToggle);
    this.view.bindEditTextInput(this.handleEditText);
    this.view.bindRemoveTodo(this.handleRemoveTodo);
    this.view.bindFilterTodo(this.handleFilterTodos);
    this.view.bindDeleteAllTodo(this.hahdleDeleteAllTodos);
    this.view.bindSearchTextTodo(this.handleSearchTodo);
    this.view.bindSaveLocalStorage(this.handleSaveLocalStorageTodo);
    this.model.getLocaleStorageTodos();
    this.onTodoListChanged(this.model.todos, this.model.completedTodo);
  }

  handleAddTodo = (todoText) => {
    this.model.addTodo(todoText);
  }

  onTodoListChanged = (todos, completedTodo) => {
    this.view.renderTodo(todos, completedTodo);
  };

  handleEditToggle = (todoId) => {
    this.model.editComplete(todoId);
  };

  handleEditText = (todoId, text) => {
    this.model.editText(todoId, text);
  };

  handleFilterTodos = (optValue) => {
    this.model.filterTodo(optValue)
  };

  hahdleDeleteAllTodos = () => {
    this.model.deleteTodos()
  };

  handleRemoveTodo = (todoId) => {
    this.model.removeTodo(todoId);
  };

  handleSearchTodo = (inputSearchValue) => {
    this.model.searchTodo(inputSearchValue);
  };

  handleSaveLocalStorageTodo = () => {
    this.model.saveLocalStorageTodo()
  }
}

const app = new Controller(new Model(), new View());
