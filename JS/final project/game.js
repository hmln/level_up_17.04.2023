 const game ={
  runing : true,
  width : 640,
  heigth : 360,
  ctx : undefined,
  platform : undefined,
  ball : undefined,
  // счетчик набранных очков
  score : 0,
  // задаем количество блоков
  rows : 1,
  cols : 15,
  blocks : [],
  img : {
    background: undefined,
    ball: undefined,
    block: undefined,
    platform: undefined
  },
  init : function(){
    const canvas=document.getElementById("cnvs");
    this.ctx=canvas.getContext("2d");
    this.ctx.font = "20px Arial";
    this.ctx.fillStyle = "#FFFFFF";

    window.addEventListener("keydown", function(e){
        if (e.keyCode == 37){
          //стрелка влево
          game.platform.dx = -game.platform.velocity;

        } else if (e.keyCode == 39){
          //стрелка вправо
          game.platform.dx = game.platform.velocity;
        } else if (e.keyCode == 32){
        //пробел - запуск шарика
          game.platform.releaseBall();
        } else if (e.keyCode == 13){
          //пауза
            alert ("Пауза");
          } 
    });
    window.addEventListener("keyup", function(e){
      game.platform.stop();
    });
  },
  load : function(){
    this.img.background = new Image();
    this.img.background.src = "/image/bg1.png";

    this.img.ball = new Image();
    this.img.ball.src = "/image/bl0.png";

    this.img.block = new Image();
    this.img.block.src = "/image/block.png";

    this.img.platform = new Image();
    this.img.platform.src = "/image/pl1.png";
  },
  create : function (){
    for (let row=0; row<this.rows; row++){
      for (let col=0; col<this.cols; col++){
        this.blocks.push({
          x : 42*col+5,
          y : 20*row+40,
          width : 40,
          heigth : 18,
          isAlive : true,
        });
      }
    }

  },
  start : function (){
    this.init();
    this.load();
    this.create();
    this.run();
  },
  render: function(){
    this.ctx.clearRect(0,0, this.width, this.heigth);
    this.ctx.drawImage(this.img.background,0,0);
    this.ctx.drawImage(this.img.platform, this.platform.x, this.platform.y);
    this.ctx.drawImage(this.img.ball, this.ball.x, this.ball.y);

    this.blocks.forEach(function(item){
      if (item.isAlive){
         this.ctx.drawImage(this.img.block, item.x, item.y );
      }
    },this);

    this.ctx.fillText("Набрано балов: "+ this.score, 15, this.heigth - 15); 
  },
  update: function(){
    if (this.ball.collide(this.platform)){
      this.ball.bumpPlatform(this.platform);
    }

    if (this.platform.dx){ 
      this.platform.move();
    };
    if (this.ball.dx || this.ball.dy){
      this.ball.move();
    };

    this.blocks.forEach(function(element){
      if (element.isAlive) {
        if (this.ball.collide(element)){
          this.ball.bumpBlock(element);
        }
      }
    },this);
    this.ball.checkBorders();
    this.platform.checkBorders();
  },
  run: function(){
    this.update();
    this.render();
    if (this.runing) {
      window.requestAnimationFrame(function(){
        game.run();
      });
    }
  },
  gameover: function(message){
    alert (message);
    this.runing=false;
    window.location.reload();
  },
};

game.ball={
  width: 10,
  heigth: 10,
  x: 325,
  y: 290,
  dx: 0,
  dy: 0,
  velocity: 3,
  jump: function(){
    this.dy = -this.velocity;
    this.dx = -this.velocity;
  },
  move: function(){
    this.x += this.dx;
    this.y += this.dy;
  },
  collide: function(element){
    const x = this.x + this.dx;
    const y = this.y + this.dy;
    if (x + this.width > element.x &&
      x <element.x + element.width &&
      y + this.heigth > element.y &&
      y < element.y +element.heigth
      )
      {
        return true;
      }
    return false;
  },
  bumpBlock: function(block){
    this.dy *= -1;
    block.isAlive = false; 
    game.score +=1;
     if (game.score >= game.blocks.length){
       game.gameover("Ты победил. Поздравляю!!!")
     };
  }, 
  onLeftSide: function(platform) {
    return (this.x + this.width/2) < (platform.x + platform.width/2);
  },
  bumpPlatform: function(platform){
     this.dy = -this.velocity;
     this.dx = this.onLeftSide(platform) ? - this.velocity : this.velocity;
  },
  checkBorders: function(){
    const x = this.x + this.dx;
    const y = this.y + this.dy;
    if (x < 0){
      this.x =0;
      this.dx = this.velocity;
    } else if (x + this.width > game.width){
        this.x = game.width - this.width;
        this.dx = -this.velocity;
    } else if ( y < 0) {
        this.y =0;
        this.dy = this.velocity;
    } else if ( y + this.heigth > game.heigth){
         //alert("GAME OVER");
         game.gameover("Ты проиграл. Игра закончена.");
    }
  },
};

game.platform={
  x: 300,
  y: 300,
  velocity: 6, 
  dx: 0,
  ball: game.ball,
  width: 60,
  heigth: 30 ,
  releaseBall: function(){
    if (this.ball) {
      this.ball.jump();
      this.ball = false;
    };
  },
  move: function(){
    this.x +=this.dx;
    if (this.ball) {this.ball.x += this.dx;};
  },
  stop: function(){
    this.dx=0;
    if (this.ball) {this.ball.dx = 0;};
  },
  checkBorders: function(){
    const x = this.x + this.dx;
    const y = this.y + this.dy;
    if (x < 0){
      this.x =0;
      this.dx = 0;
    } else if (x + this.width > game.width){
        this.x = game.width - this.width;
        this.dx = 0;
    }
  }
};


window.addEventListener("load",()=>{
  game.start();
});

